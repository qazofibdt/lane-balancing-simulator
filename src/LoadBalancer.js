import assert from 'assert';
import SignedZeroMath from './SignedZeroMath';

function getNetFiniteRate(rates) {
  return rates.reduce((acc, rate) => acc + (Number.isFinite(rate) ? rate : 0), 0);
}

function getCapRate(targetRates) {
  // We find which rates must be capped by iterating from highest-target-rate down, calculating
  // the rate reclaimed by capping at each target rate. When we reach an index for which the rate
  // reclaimed is higher than the net target rate, all rates prior to that index must be capped,
  // and all rates after it can meet their target rate. The exact cap rate can then be calculated
  // by dividing the remaing rate among the capped rates

  let sortedTargetRates = [...targetRates];
  sortedTargetRates.sort((a, b) => a - b).reverse();

  const netTargetRate = sortedTargetRates.reduce((acc, rate) => acc + rate);

  let firstUncappedIndex = null;
  let rateReclaimedByCapping = 0;
  {
    let i = 0;
    while (i < sortedTargetRates.length - 1
        && Math.abs(rateReclaimedByCapping) < Math.abs(netTargetRate)) {
      let diff = sortedTargetRates[i] - sortedTargetRates[i+1];
      rateReclaimedByCapping += diff * (i + 1);

      i++;
    }

    firstUncappedIndex = i;
  }

  if (Math.abs(rateReclaimedByCapping) < Math.abs(netTargetRate)) {
    // This should only happen if all rates have the same polarity
    assert(sortedTargetRates.every(
        targetRate => SignedZeroMath.sign(targetRate) === SignedZeroMath.sign(netTargetRate)));

    return SignedZeroMath.sign(netTargetRate) * 0;
  }

  // The cap rate is the rate of the first uncapped rate, plus any extra to be distributed among
  // the capped rates
  let capRate = sortedTargetRates[firstUncappedIndex]
  const numCappedRates = firstUncappedIndex;
  if (numCappedRates > 0) {
    capRate += (rateReclaimedByCapping - netTargetRate) / numCappedRates;
  }

  return capRate;
}

function mapInfiniteToFiniteRates(rates) {
  let infiniteRates = rates.filter(rate => !Number.isFinite(rate));

  let simulatedRate = null;
  if (infiniteRates.length > 0) {
    let infinitePolarity = SignedZeroMath.sign(infiniteRates[0]);

    if (!infiniteRates.every(rate => SignedZeroMath.sign(rate) === infinitePolarity)) {
      return { hasPositiveAndNegativeInfinities: true };
    }

    // Calculate the rate we should simulate
    simulatedRate = infinitePolarity * 0;
    for (const rate of rates) {
      if (Number.isFinite(rate) && SignedZeroMath.sign(rate) !== infinitePolarity) {
        simulatedRate += -rate;
      }
    }
  }

  // Return rates with infinites (if any) replaced with the simulate rates
  return rates.map(rate => Number.isFinite(rate) ? rate : simulatedRate);
}

// Returns an array of rates balanced such that (rules in order of precedence):
//  - Each rate is in the range [0, target rate]
//  - All rates sum to zero
//  - The throughput (sum of the magnitudes of all rates) is maximized
//  - The difference between the highest and lowest rates is minimized
// Assumes that:
//  - All infinite target rates have the same polarity
function balanceLoad(targetRates) {
  let netTargetRate = getNetFiniteRate(targetRates);

  // Replace infinite target rates with big-enough finite ones, to make the algorithm simpler. All
  // infinite target rates must have the same polarity. Big-enough means: sufficient to satisfy the
  // sum of all target rates of the opposite polarity
  let ret = mapInfiniteToFiniteRates(targetRates);

  // If there are positive and negative infinities, all target rates can be achieved. This should
  // be a zero-duration transient state in most networks
  if (ret.hasPositiveAndNegativeInfinities) {
    return targetRates;
  }

  targetRates = ret;

  // Recalculate the net rate now that we've replaced infinite rates with finite ones
  netTargetRate = getNetFiniteRate(targetRates);

  // If the net rate is negative, invert all target rates. This allows us to write the algorithm
  // for one polarity and apply it to both
  let inverted = false;
  if (netTargetRate < 0) {
    targetRates = targetRates.map(rate => -rate);
    assert(getNetFiniteRate(targetRates) === -netTargetRate);
    inverted = true;
  }

  let balancedRates = balanceLoad_PreppedRates(targetRates);

  // If we inverted the target rates, invert the returned balanced rates
  if (inverted) {
    balancedRates = balancedRates.map(rate => -rate);
  }

  return balancedRates;
}

// Same behavior as balancePorts, except it assumes the following of the given rates:
//  - The net target rate is non-negative
//  - All target rates are finite
function balanceLoad_PreppedRates(targetRates) {
  assert(targetRates.length !== 0);

  let netTargetRate = getNetFiniteRate(targetRates);

  assert(netTargetRate >= 0);
  assert(targetRates.every(rate => Number.isFinite(rate)));

  // We maximize throughput while minimizing the spread of rates by capping the highest rates. Start
  // by finding what the cap value needs to be
  const capRate = getCapRate(targetRates);
  assert(capRate >= 0);

  // Rates over the cap get capped
  let balancedRates = targetRates.map(rate => Math.min(rate, capRate));

  return balancedRates;
}

export { balanceLoad };
