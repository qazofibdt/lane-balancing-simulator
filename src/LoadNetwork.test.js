import { Supply, Router } from './LoadNetwork';

function InstantScheduler() {
  this.queue = [];

  this.schedule = (delay, action) => {
    this.queue.push(action);
  }

  this.flush = () => {
    while (this.queue.length !== 0) {
      let action = this.queue.shift();
      action();
    }
  }
}

it('resolves net-positive two-router network', () => {
  let scheduler = new InstantScheduler();

  let sourceA = new Supply(scheduler, 10);
  let sourceB = new Supply(scheduler, 90);

  let sinkA = new Supply(scheduler, -20);
  let sinkB = new Supply(scheduler, -40);

  let routerA = new Router(scheduler, 2, 2);
  let routerB = new Router(scheduler, 2, 2);

  routerA.sinks[0].connect(sourceA.port);
  routerA.sinks[1].connect(sourceB.port);
  routerA.sources[0].connect(routerB.sinks[0]);
  routerA.sources[1].connect(routerB.sinks[1]);
  routerB.sources[0].connect(sinkA.port);
  routerB.sources[1].connect(sinkB.port);

  scheduler.flush();

  expect(sourceA.port.getRate()).toEqual(10);
  expect(sourceB.port.getRate()).toEqual(null);

  expect(routerA.sinks[0].getRate()).toEqual(null);
  expect(routerA.sinks[1].getRate()).toEqual(-50);
  expect(routerA.sources[0].getRate()).toEqual(null);
  expect(routerA.sources[1].getRate()).toEqual(null);

  expect(routerB.sinks[0].getRate()).toEqual(-30);
  expect(routerB.sinks[1].getRate()).toEqual(-30);
  expect(routerB.sources[0].getRate()).toEqual(null);
  expect(routerB.sources[1].getRate()).toEqual(null);

  expect(sinkA.port.getRate()).toEqual(-20);
  expect(sinkB.port.getRate()).toEqual(-40);
});

it('resolves net-negative two-router network', () => {
  let scheduler = new InstantScheduler();

  let sourceA = new Supply(scheduler, 30);
  let sourceB = new Supply(scheduler, 50);

  let sinkA = new Supply(scheduler, -60);
  let sinkB = new Supply(scheduler, -40);

  let routerA = new Router(scheduler, 2, 2);
  let routerB = new Router(scheduler, 2, 2);

  routerA.sinks[0].connect(sourceA.port);
  routerA.sinks[1].connect(sourceB.port);
  routerA.sources[0].connect(routerB.sinks[0]);
  routerA.sources[1].connect(routerB.sinks[1]);
  routerB.sources[0].connect(sinkA.port);
  routerB.sources[1].connect(sinkB.port);

  scheduler.flush();

  expect(sourceA.port.getRate()).toEqual(30);
  expect(sourceB.port.getRate()).toEqual(50);

  expect(routerA.sinks[0].getRate()).toEqual(null);
  expect(routerA.sinks[1].getRate()).toEqual(null);
  expect(routerA.sources[0].getRate()).toEqual(40);
  expect(routerA.sources[1].getRate()).toEqual(40);

  expect(routerB.sinks[0].getRate()).toEqual(null);
  expect(routerB.sinks[1].getRate()).toEqual(null);
  expect(routerB.sources[0].getRate()).toEqual(40);
  expect(routerB.sources[1].getRate()).toEqual(null);

  expect(sinkA.port.getRate()).toEqual(null);
  expect(sinkB.port.getRate()).toEqual(-40);
});

