import assert from 'assert';
import { Port, PortSign } from './LoadPort';
import { balanceLoad } from './LoadBalancer';
import SignedZeroMath from './SignedZeroMath';

// Returns an array of target rates corresponding to the given array of ports. Unconnected ports are
// given target rates of +/- 0, and ports whose target rates are constrained are given +/-
// infinity
function getEffectiveTargetRates(ports) {
  return ports.map(port => {
    // We only support one-directional ports at the moment
    assert(port.getSign() !== null);
    if (port.getTarget() === null) {
      return -port.getSign() * 0;
    } else if (port.getTarget().getRate() === null) {
      return -port.getSign() * Infinity;
    } else {
      return port.getTarget().getRate();
    }
  });
}

// Given balanced rates and the original target ports, returns the rates that should be applied to
// the local ports
function getApplicableRates(balancedRates, targetPorts) {
  return balancedRates.map((rate, i) => {
    let targetPort = targetPorts[i];
    if (targetPort !== null && rate === targetPort.getRate()) {
      return null;
    } else {
      return -rate;
    }
  });
}

function NetworkComponent(scheduler, numPorts) {
  this._ports = [];
  this._scheduler = scheduler;
  // Indicates that this component is not up-to-date with its inputs and/or outputs
  this._dirty = true;

  for (let i = 0; i < numPorts; i++) {
    let port = new Port();
    port.onTargetRateChanged = () => {
      // Null indicates that the other port has accepted our requested rate, so there's no need to update
      if (port.getTarget().getRate() === null) {
        return;
      }

      this._dirty = true;
      this._scheduleRateUpdate();
    }
    port.onConnected = () => this._scheduleRateUpdate();
    port.onDisconnected = () => this._scheduleRateUpdate();
    this._ports.push(port);
  }

  this._scheduleRateUpdate = () => {
    this._scheduler.schedule(0, () => {
      this._updateRates();
    });
  }

  this._updateRates = () => {
    if (!this._dirty) {
      return
    }
    this._dirty = false;

    let newRates = this._getNewRates();

    for (let i = 0; i < this._ports.length; i++) {
      let port = this._ports[i];
      let newRate = newRates[i];
      if (port.getRate() !== newRate) {
        port.setRate(newRate)
      }
    }
  };

  this._getNewRates = () => {
    let effectiveTargetRates = getEffectiveTargetRates(this._ports);
    let balancedTargetRates = balanceLoad(effectiveTargetRates);
    let targetPorts = this._ports.map(port => port.getTarget());
    return getApplicableRates(balancedTargetRates, targetPorts);
  };
}

function Supply(scheduler, capacity) {
  NetworkComponent.call(this, scheduler, 1);

  this.capacity = capacity;
  this.port = this._ports[0];
  // Supplied are one-directional for now
  this.port.setSign(SignedZeroMath.sign(capacity));
  this.port.setRate(capacity);

  this.setCapacity = (capacity) => {
    this.capacity = capacity;
    this._scheduleRateUpdate();
  }

  this._getNewRates = () => {
    let newRate;

    if (this.port.getTarget() !== null
        && this.port.getTarget().getRate() !== null
        && Math.abs(this.port.getTarget().getRate()) < Math.abs(this.capacity)) {
      // Rate driven by target
      newRate = null;
    } else {
      // Rate driven by this
      newRate = this.capacity;
    }

    return [newRate];
  };
}

// Routes any number of source ports to any number of sink ports
function Router(scheduler, numSources, numSinks) {
  let numPorts = numSources + numSinks;
  NetworkComponent.call(this, scheduler, numPorts);
  this.sources = Object.freeze(this._ports.slice(0, numSources));
  this.sinks = Object.freeze(this._ports.slice(numSources, numPorts));

  for (let source of this.sources) {
    source.setSign(PortSign.Source);
    source.setRate(0);
  }

  for (let sink of this.sinks) {
    sink.setSign(PortSign.Sink);
    sink.setRate(null);
  }
}

function Conveyor(scheduler, throughput, latency) {
  NetworkComponent.call(this, scheduler, 2, throughput);

  this.throughput = throughput
  this.latency = latency;

  this.source = this._ports[0];
  this.sink = this._ports[1];

  this.source.setSign(PortSign.Source);
  this.source.setRate(0);
  this.sink.setSign(PortSign.Sink);
  this.sink.setRate(-this.throughput);

  this._getNewRates = () => {
    let effectiveTargetRates = getEffectiveTargetRates(this.ports);
    // Cap effective target rates at the throughput
    effectiveTargetRates = effectiveTargetRates.map(
        rate => SignedZeroMath.sign(rate) * Math.min(Math.ab(rate), this.throughput));
    let balancedTargetRates = balanceLoad(effectiveTargetRates);
    let targetPorts = this.ports.map(port => port.getTarget());
    return getApplicableRates(balancedTargetRates, targetPorts);
  };
}

export { Supply, Router, Conveyor };
