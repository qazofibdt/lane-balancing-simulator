import { balanceLoad } from './LoadBalancer';

it('caps larger sink', () => {
  expect(balanceLoad([50, -100])).toEqual([50, -50]);
});

it('caps larger source', () => {
  expect(balanceLoad([100, -50])).toEqual([50, -50]);
});

it('does nothing to two already-balanced rates', () => {
  expect(balanceLoad([100, -100])).toEqual([100, -100]);
});

it('does nothing to four already-balanced rates', () => {
  expect(balanceLoad([10, 40, -20, -30])).toEqual([10, 40, -20, -30]);
});

it('caps larger sink at zero', () => {
  expect(balanceLoad([0, -100])).toEqual([0, -0]);
});

it('caps larger source at zero', () => {
  expect(balanceLoad([100, -0])).toEqual([0, -0]);
});

it('caps one of four rates', () => {
  expect(balanceLoad([90, 10, -10, -30])).toEqual([30, 10, -10, -30]);
});

it('caps one at same rate as another', () => {
  expect(balanceLoad([100, 50, -50, -50])).toEqual([50, 50, -50, -50]);
});

it('limits infinite sink to source rate', () => {
  expect(balanceLoad([100, -Infinity])).toEqual([100, -100]);
});

it('limits infinite source to sink rate', () => {
  expect(balanceLoad([-100, Infinity])).toEqual([-100, 100]);
});

it('splits one source to two infinite sinks', () => {
  expect(balanceLoad([100, -Infinity, -Infinity])).toEqual([100, -50, -50]);
});

it('splits one sink to two infinite sources', () => {
  expect(balanceLoad([-100, Infinity, Infinity])).toEqual([-100, 50, 50]);
});

it('merges two sources to one infinite sink', () => {
  expect(balanceLoad([100, 100, -Infinity])).toEqual([100, 100, -200]);
});

it('merges two sinks to one infinite source', () => {
  expect(balanceLoad([-100, -100, Infinity])).toEqual([-100, -100, 200]);
});

it('caps two sources, with no sinks, at zero', () => {
  expect(balanceLoad([100, 100])).toEqual([0, 0]);
});

it('caps two sinks, with no sources, at zero', () => {
  expect(balanceLoad([-100, -100])).toEqual([-0, -0]);
});

it('caps one finite source and one infinite source, with no sinks, at zero', () => {
  expect(balanceLoad([100, Infinity])).toEqual([0, 0]);
});

it('caps one finite sink and one infinite sink, with no sources, at zero', () => {
  expect(balanceLoad([-100, -Infinity])).toEqual([-0, -0]);
});

it('caps two infinite sources, with no sinks, at zero', () => {
  expect(balanceLoad([Infinity, Infinity])).toEqual([0, 0]);
});

it('caps two infinite sinks, with no sources, at zero', () => {
  expect(balanceLoad([-Infinity, -Infinity])).toEqual([-0, -0]);
});

it('splits one source evenly between one finite and one infinite sink', () => {
  expect(balanceLoad([100, -100, -Infinity])).toEqual([100, -50, -50]);
});

it('splits one sink evenly between one finite and one infinite source', () => {
  expect(balanceLoad([-100, 100, Infinity])).toEqual([-100, 50, 50]);
});

