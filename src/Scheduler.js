import PriorityQueue from 'js-priority-queue'
PriorityQueue.prototype.enqueue = function() { this.queue.apply(this, arguments); };

function Scheduler() {
  this.pqueue = new PriorityQueue({ comparator: (a, b) => a.tick - b.tick });
  this.currentTick = 0;

  this.schedule = function(delay, action) {
    this.pqueue.enqueue({tick: this.currentTick + delay, action: action});
  };

  this.getNext = function() {
    let next = this.pqueue.dequeue();
    let ret = {delay: next.tick - this.currentTick, action: next.action};
    this.currentTick = next.tick;
    return ret;
  }
}

export default Scheduler;
