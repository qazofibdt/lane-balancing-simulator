import React from 'react';
import logo from './logo.svg';
import './App.css';

import { Supply, Router } from './LoadNetwork';

function InstantScheduler() {
  this.queue = [];

  this.schedule = (delay, action) => {
    this.queue.push(action);
  }

  this.flush = () => {
    while (this.queue.length !== 0) {
      let action = this.queue.shift();
      action();
    }
  }
}

function App() {
  let scheduler = new InstantScheduler();

  let sourceA = new Supply(scheduler, 10);
  let sourceB = new Supply(scheduler, 90);

  let sinkA = new Supply(scheduler, -20);
  let sinkB = new Supply(scheduler, -40);

  let routerA = new Router(scheduler, 2, 2);
  let routerB = new Router(scheduler, 2, 2);

  routerA.sinks[0].connect(sourceA.port);
  routerA.sinks[1].connect(sourceB.port);
  routerA.sources[0].connect(routerB.sinks[0]);
  routerA.sources[1].connect(routerB.sinks[1]);
  routerB.sources[0].connect(sinkA.port);
  routerB.sources[1].connect(sinkB.port);

/*
  let source = new Supply(scheduler, 20);
  let sink = new Supply(scheduler, -10);
  let router = new Router(scheduler, 1, 1);

  router.sinks[0].connect(source.port);
  router.sources[0].connect(sink.port);

  sink.setCapacity(-30);
*/

  scheduler.flush();

  //console.log(routerA);
  //console.log(routerB);

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
