import { Port, PortSign } from './LoadPort';

it('doubly-links on connect', () => {
  const a = new Port(100, PortSign.Source);
  const b = new Port(-100, PortSign.Sink);

  a.connect(b);

  expect(a._target).toBe(b);
  expect(b._target).toBe(a);
});

it('allows different-sign connection', () => {
  const a = new Port(null, PortSign.Source);
  const b = new Port(null, PortSign.Sink);

  expect(() => { a.connect(b); }).not.toThrow();
});

it('rejects same-sign connection', () => {
  const a = new Port(null, PortSign.Source);
  const b = new Port(null, PortSign.Source);

  expect(() => { a.connect(b); }).toThrow();
});

