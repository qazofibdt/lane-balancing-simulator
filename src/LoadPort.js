import assert from 'assert';
import SignedZeroMath from './SignedZeroMath';

const PortSign = Object.freeze({
  Source: 1,
  Sink: -1,
  Bidirectional: null
});

// A port has a rate and a sign, and can be connected to a target port. A port's rate is null if
// it is unspecified or if it is constrained by the target port. A port's sign is null if and only
// if the port can accept positive or negative rates. If a port's sign and rate are both non-null,
// their signs must be the same
function Port(rate=null, sign=null) {
  if (rate !== null && sign !== null) {
    assert(SignedZeroMath.sign(rate) === sign);
  }

  this._target = null;

  this._rate = rate;
  assert(Object.values(PortSign).includes(sign));
  this._sign = sign;

  this.onTargetRateChanged = (oldRate) => {};
  this.onConnected = () => {};
  this.onDisconnected = () => {};

  this.connect = (port) => {
    assert(this._sign === null || this._sign !== port._sign);

    if (port._target !== null) {
      port._target.disconnect();
      this.disconnect();
    }

    port._target = this;
    this._target = port;

    this.onConnected();
  };

  this.disconnect = () => {
    this._target = null;
    this.onDisconnected();
  }

  this.getTarget = () => this._target;

  this.setRate = (rate) => {
    if (this._sign !== null && rate !== null) {
      assert(this._sign === SignedZeroMath.sign(rate));
    }
    let oldRate = this._rate;
    if (rate !== oldRate) {
      this._rate = rate;
      if (this._target !== null) {
        this._target.onTargetRateChanged(oldRate);
      }
    }
  };

  this.getRate = () => this._rate;

  this.setSign = (sign) => {
    this._sign = sign;
  }

  this.getSign = () => this._sign;
}

export { Port, PortSign };
