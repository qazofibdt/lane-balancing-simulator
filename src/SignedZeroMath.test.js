import SignedZeroMath from './SignedZeroMath'

it('returns -1 for -0', () => {
  expect(SignedZeroMath.sign(-0)).toEqual(-1);
});

it('returns +1 for +0', () => {
  expect(SignedZeroMath.sign(0)).toEqual(1);
});

it('returns -1 for -10', () => {
  expect(SignedZeroMath.sign(-10)).toEqual(-1);
});

it('returns +1 for +10', () => {
  expect(SignedZeroMath.sign(10)).toEqual(1);
});

it('returns -1 for -Infinity', () => {
  expect(SignedZeroMath.sign(-Infinity)).toEqual(-1);
});

it('returns +1 for +Infinity', () => {
  expect(SignedZeroMath.sign(Infinity)).toEqual(1);
});

