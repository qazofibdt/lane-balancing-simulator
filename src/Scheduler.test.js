import Scheduler from './Scheduler'

it('returns scheduled items in order', () => {
  let scheduler = new Scheduler();
  scheduler.schedule(1, 'first');
  scheduler.schedule(2, 'second');

  expect(scheduler.getNext().action).toEqual('first');
  expect(scheduler.getNext().action).toEqual('second');
});

it('returns a delay of zero for the second of two simultaneous events', () => {
  let scheduler = new Scheduler();
  scheduler.schedule(5, null);
  scheduler.schedule(5, null);

  expect(scheduler.getNext().delay).toEqual(5);
  expect(scheduler.getNext().delay).toEqual(0);
});

it('returns correct delay after push->pop->push', () => {
  let scheduler = new Scheduler();
  scheduler.schedule(2, null);

  expect(scheduler.getNext().delay).toEqual(2);

  scheduler.schedule(1, null);

  expect(scheduler.getNext().delay).toEqual(1);
});

it('returns higher-priority event added last', () => {
  let scheduler = new Scheduler();
  scheduler.schedule(10, 'later');
  scheduler.schedule(1, 'sooner');

  expect(scheduler.getNext().action).toEqual('sooner');
});

it('returns simultaneous events FIFO', () => {
  let scheduler = new Scheduler();
  scheduler.schedule(0, 'A');
  scheduler.schedule(0, 'B');

  expect(scheduler.getNext().action).toEqual('A');
  expect(scheduler.getNext().action).toEqual('B');
});

