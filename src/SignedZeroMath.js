export default {
  sign: function sign(num) {
    let norm = num === 0 ? 1/num : num;

    return (norm < 0) ? -1 : 1;
  }
};
